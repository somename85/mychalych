/**
 * Created by blackbookman on 10.03.15.
 */
var Mongodb = require('./lib/DBConnectors/mongodb').Mongodb;
var eventStream = require('./lib/eventStream').eventStream;

var Sites = require('./lib/models/sites').Sites;
var Pages = require('./lib/models/pages').Pages;
var Scripts = require('./lib/models/scripts').Scripts;
var Users = require('./lib/models/users').Users;
var Tags = require('./lib/models/tags').Tags;

var mongodb = new Mongodb();

var sites = new Sites(mongodb);
var pages = new Pages(mongodb);
var scripts = new Scripts(mongodb);
var users = new Users(mongodb);
var tags = new Tags(mongodb);

var http = require('http');
var fs = require('fs');
var Promise = require('promise');
var dot = require('dot');

var server = require('http').createServer(handler);
var io = require('socket.io')(server);

var template = fs.readFileSync(__dirname + '/views/template.html', 'utf8');
var panel = fs.readFileSync(__dirname + '/views/panel.html', 'utf8');

function handler(req, res) {
    if (req.headers.host === 'localhost:3000') {
        res.writeHead(200);
        res.end(panel);
    } else {
        Promise
            .all([
                sites.find({type: 'sites', body: [{domain: req.headers.host}]}),
                pages.find({type: 'pages', body: [{domain: req.headers.host, url: req.url}]})
            ])
            .then(function(results) {
                var site = results[0][0];
                var page = results[1][0];

                if (
                    !site
                    || !page
                    || !site.enable
                    || !page.enable
                ) {
                    res.writeHead(404);
                    res.end('404');
                }

                var tags = [].concat(site.tags, page.tags);

                scripts
                    .find({type: 'scripts'})
                    .then(function(scripts) {

                        res.writeHead(200);
                        res.write(dot.template(template)({body: page.body, tags: tags}));
                        res.end();
                    }, function(err) {
                        console.log(err.stack);
                        res.writeHead(500);
                        res.end('500');
                    });

            }, function(err) {
                console.log(err.stack);
                res.writeHead(500);
                res.end('500');
            });
    }
}

io.on('connection', function (socket) {
    socket.on('add', eventStream.add);
    socket.on('rm', eventStream.rm);
    socket.on('edit', eventStream.edit);

    eventStream.on('fail', function(data) {
        socket.emit('fail', data);
    });
    eventStream.on('success', function(data) {
        socket.emit('success', data);
    });
});

server.listen(3000, function() {
    console.log('server started on 3000 port');
});
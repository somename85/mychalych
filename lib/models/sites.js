/**
 * Created by blackbookman on 01.03.15.
 */
var util = require('util');
var Model = require('./model').Model;
var f = require('../functions');

function Sites(connector) {
    Sites.super_.apply(this, arguments);

    var self = this;

    self.type = 'sites';
    self.connector = connector;

    self.defaults = {
        tags: [],
        enable: false
    };

    self.init();
}

util.inherits(Sites, Model);

exports.Sites = Sites;
/**
 * Created by blackbookman on 10.03.15.
 */
var util = require('util');
var Model = require('./model').Model;

function Users(connector) {
    Users.super_.apply(this, arguments);

    var self = this;

    self.type = 'users';
    self.connector = connector;

    self.init();
}

util.inherits(Users, Model);

exports.Users = Users;
/**
 * Created by blackbookman on 22.03.15.
 */
var util = require('util');
var Model = require('./model').Model;

function Tags(connector) {
    Tags.super_.apply(this, arguments);

    var self = this;

    self.type = 'tags';
    self.connector = connector;

    //todo: remove ???

    self._addTags = function(data) {
        if (data.command !== 'add' && data.command !== 'edit') return;
        var tags = data.body[0].tags;

        if (!tags) return;

        var tagObjs = [],
            i = tags.length;

        while (i--) tagObjs.push({name: tags[i]});

        self.add({type: self.type, body: tagObjs});
    }

    self.eventStream.on('success', self._addTags);

    self.init();
}

util.inherits(Tags, Model);

exports.Tags = Tags;
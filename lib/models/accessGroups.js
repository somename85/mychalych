/**
 * Created by blackbookman on 10.03.15.
 */
var util = require('util');
var Model = require('./model').Model;

function AccessGroups(connector) {
    AccessGroups.super_.apply(this, arguments);

    var self = this;

    self.type = 'accessGroups';
    self.connector = connector;

    self.init();
}

util.inherits(AccessGroups, Model);

exports.AccessGroups = AccessGroups;
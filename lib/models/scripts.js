/**
 * Created by blackbookman on 09.03.15.
 */
var util = require('util');
var Model = require('./model').Model;

function Scripts(connector) {
    Scripts.super_.apply(this, arguments);

    var self = this;

    self.type = 'scripts';
    self.connector = connector;

    self.init();
}
util.inherits(Scripts, Model);

exports.Scripts = Scripts;
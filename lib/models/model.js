/**
 * Created by blackbookman on 09.03.15.
 */
var util = require('util');
var Promise = require('promise');

var eventStream = require('../eventStream').eventStream;
var f = require('../functions');

function Model(connector) {
    var self = this;

    self.type = 'models';
    self.subscribeStatus = false;
    self.connector = connector;
    self.eventStream = eventStream;

    self._addEventStream = function(func1, func2) {
        return function(command) {
            return func1(func2(command), command.name);
        }
    }

    self._setDefaults = function(func) {
        return function(command) {
            command.defaults = {};
            f.processObjWithRef(self.defaults, command.defaults);
            return func(command);
        }
    }

    self._eventStreamHandler = function(promise, commandName) {
        if (!promise) return;

        return promise.then(function(result) {
            self.eventStream.success({type: self.type, command: commandName, body: result});
            return Promise.resolve(result);
        }, function(err) {
            self.eventStream.fail({type: self.type, command: commandName, body: err});
            return Promise.reject(err);
        })
    }

    self.add = function(command) {
        if (command.type !== self.type) return;
        if (!util.isObject(command.body)) return Promise.reject(new Error('bad args'));

        command.name = 'add';

        if (command.defaults) f.processObjWithRef(command.body, command.defaults);

        return self.connector.insert(self.type, command.defaults || command.body);
    }

    self.edit = function(command) {
        if (command.type !== self.type) return;

        //todo: may be should save command.body in short variable, like 'b'???

        if (
            !util.isArray(command.body)
            || !util.isObject(command.body[0])
            || !util.isObject(command.body[1])
        ) return Promise.reject(new Error('bad args'));

        command.name = 'edit';

        return self.connector.update(self.type, command.body[0], command.body[1], command.body[2]);
    }

    self.find = function(command) {
        if (command.type !== self.type) return;
        if (command.body && !util.isArray(command.body)) return Promise.reject(new Error('bad args'));

        var objQuery = command.body ? command.body[0] : {};
        var amount = command.body ? command.body[1] : 0;

        command.name = 'find';

        return self.connector.find(self.type, objQuery, amount);
    }

    self.rm = function(command) {
        if (command.type !== self.type) return;

        var b = command.body;

        if (b && (!util.isArray(b) || !util.isObject(b[0]))) return Promise.reject(new Error('bad args'));

        var objQuery = b ? b[0] : {};
        var isOne = b ? b[1] : false;

        command.name = 'rm';

        return self.connector.remove(self.type, objQuery, isOne);
    }

    self.subscribe = function() {
        self.eventStream
            .on('find', self.find)
            .on('add', self.add)
            .on('edit', self.edit)
            .on('rm', self.rm);

        self.subscribeStatus = true;
    }

    self.unsubscribe = function() {
        self.eventStream
            .removeListener('find', self.find)
            .removeListener('make', self.add)
            .removeListener('change', self.edit)
            .removeListener('del', self.rm);

        self.subscribeStatus = false;
    }

    self.init = function() {
        if (self.defaults) self.add = self._setDefaults(self.add);
        self.connector.prepare(self.type);
        self.subscribe();
        return self;
    }

    self.add = self._addEventStream(self._eventStreamHandler, self.add);
    self.edit = self._addEventStream(self._eventStreamHandler, self.edit);
    self.find = self._addEventStream(self._eventStreamHandler, self.find);
    self.rm = self._addEventStream(self._eventStreamHandler, self.rm);
}

exports.Model = Model;
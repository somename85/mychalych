/**
 * Created by blackbookman on 09.03.15.
 */
var util = require('util');
var Model = require('./model').Model;

function Pages(connector) {
    Pages.super_.apply(this, arguments);

    var self = this;

    self.type = 'pages';
    self.connector = connector;

    self.defaults = {
        tags: [],
        enable: false
    };

    self.init();
}

util.inherits(Pages, Model);

exports.Pages = Pages;
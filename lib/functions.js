/**
 * Created by blackbookman on 04.03.15.
 */
exports.errHandler = function(obj, commandName, err) {
    if (typeof err === 'string') err = new Error(err);
    //console.log(err.stack);

    if (obj.type) {
        obj.eventStream.fail({type: obj.type, command: commandName, body: err});
    } else {
        obj.emit('fail', {type: null, command: commandName, body: err});
        return obj;
    }
};

exports.cd = function(data, type, command) {
    if (!command) return data.type === type;
    return data.type === type && data.command === command;
};

exports.processObjWithRef = function processObjWithRef(obj, result) {
    //https://gist.github.com/Trindaz/2234277
    if(obj==null || typeof obj != 'object'){
        //nothing really to do here - you're going to lose the reference to result if you try an assignment
    }
    if(obj instanceof Array) {
        for(var i=0, len = obj.length; i<len; i++){
            result.push();
            processObjWithRef(obj[i], result[i]);
        }
    }
    if(obj instanceof Object){
        for(var k in obj){
            var count=0;
            if(obj[k]==null || typeof obj[k] != 'object'){
                result[k] = obj[k];
            }else if(obj[k] instanceof Array) {
                result[k] = [];
                processObjWithRef(obj[k], result[k]);
            }else if(obj[k] instanceof Object){
                result[k] = {};
                for( var attr in obj[k]){
                    processObjWithRef(obj[k], result[k]);
                }
            }
        }
    }
};
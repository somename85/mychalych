/**
 * Created by blackbookman on 01.03.15.
 */
var util = require('util');
var events = require('events');

var f = require('./functions');

function EventStream() {
    var self = this;
    EventStream.super_.apply(self, arguments);

    self.add = function(data) {
        if (!util.isObject(data) || !data.type) return f.errHandler(self, 'add', 'bad args: ' + data);
        self.emit('add', data);
        return self;
    }
    self.rm = function(data) {
        if (!util.isObject(data) || !data.type) return f.errHandler(self, 'rm', 'bad args: ' + data);
        self.emit('rm', data);
        return self;
    }
    self.edit = function(data) {
        if (!util.isObject(data) || !data.type) return f.errHandler(self, 'edit', 'bad args: ' + data);
        self.emit('edit', data);
        return self;
    }
    self.success = function(data) {
        if (!util.isObject(data) || !data.type) return f.errHandler(self, 'success', 'bad args: ' + data);
        self.emit('success', data);
        return self;
    }
    self.fail = function(data) {
        if (!util.isObject(data) || !data.type) return f.errHandler(self, 'fail', 'bad args: ' + data);
        self.emit('fail', data);
        return self;
    }
    self.find = function(data) {
        if (!util.isObject(data) || !data.type) return f.errHandler(self, 'find', 'bad args: ' + data);
        self.emit('find', data);
        return self;
    }
    self.cb = function(cb) {
        process.nextTick(cb);
        return self;
    }
}

util.inherits(EventStream, events.EventEmitter);

exports.eventStream = new EventStream();
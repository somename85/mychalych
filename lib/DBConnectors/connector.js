/**
 * Created by blackbookman on 01.03.15.
 */
var util = require('util');
var func = require('../functions');

function Connector() {
    var self = this;

    self.insert = function(type, smth) {
        return new Promise(function(fulfill, reject) {
            if (!util.isObject(smth) || !type) {
                reject(new Error('bad args'));
                return;
            }

            if (util.isArray(smth)) {
                var i = smth.length;

                while (i--) {
                    if (!util.isObject(smth[i])) {
                        reject(new Error('bad args'));
                        return;
                    }
                }
            }

            self._insert(type, smth, fulfill, reject);
        })
    };

    self.update = function(type, obj, data, isMulti) {
        return new Promise(function(fulfill, reject) {
            if (
                !type
                || !util.isObject(obj)
                || !util.isObject(data)
            ) {
                reject(new Error('bad args'));
                return;
            }

            isMulti = isMulti || false;

            self._update(type, obj, data, isMulti, fulfill, reject);
        });
    }

    self.find = function(type, obj, amount) {
        return new Promise(function(fulfill, reject) {
            if (
                !type
                || !util.isObject(obj)
            ) {
                reject(new Error('bad args'));
                return;
            }

            if (amount !== 0) amount = amount || 1;

            self._find(type, obj, amount, fulfill, reject);
        })
    }

    self.remove = function(type, obj, isOne) {
        return new Promise(function(fulfill, reject) {
            if (
                !type
                || !util.isObject(obj)
            ) {
                reject(new Error('bad args'));
                return;
            }

            isOne = isOne || false;

            self._remove(type, obj, isOne, fulfill, reject);
        })
    }

    self.prepare = function(type) {}
}

exports.Connector = Connector;
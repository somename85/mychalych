/**
 * Created by blackbookman on 11.03.15.
 */
var util = require('util');
var MongoClient = require('mongodb').MongoClient;
var Promise = require('promise');

var Connector = require('./connector').Connector;
var func = require('../functions');
var config = require('../../config');
var url = config.mongoPath;

MongoClient.connect(url, function(err, database) {
    if (err) throw err;
    Mongodb.prototype.db = database;
    // I do not call functions here,
    // because only this class should know about mongodb.
    // And because the instances of this class are used
    // to initialize other classes (models).
    // I do not want to do it in callback or promise.
});


function Mongodb() {
    var self = this;

    Mongodb.super_.apply(self, arguments);

    self._insert = function(type, smth, fulfill, reject) {
        if (!self.db) {
            reject(new Error('db not ready yet'));
            return;
        }

        var collection = self.db.collection(type);

        collection.insert(smth, {w: 1}, function(err, result) {
            if (err) reject(err);
            else fulfill(result);
        });
    }

    self._update = function(type, obj, data, isMulti, fulfill, reject) {
        if (!self.db) {
            reject(new Error('db not ready yet'));
            return;
        }

        var collection = self.db.collection(type);

        collection.update(obj, {$set: data}, {w: 1, multi: isMulti}, function(err, updatedDocNum, resultStatus) {
            if (err) reject(err);
            else fulfill(resultStatus);
        });
    }

    self._find = function(type, obj, amount, fulfill, reject) {
        if (!self.db) {
            reject(new Error('db not ready yet'));
            return;
        }

        var collection = self.db.collection(type);
        var results = [];

        collection.find(obj).limit(amount).toArray(function(err, result) {
            if (err) reject(err);
            else fulfill(result);
        });
    }

    self._remove = function(type, obj, isOne, fulfill, reject) {
        if (!self.db) {
            reject(new Error('db not ready yet'));
            return;
        }

        var collection = self.db.collection(type);

        //todo: i think isOne don`t work here, should check out
        collection.remove(obj, {justOne: isOne}, function(err, deletedDocNum, state) {
            if (err) reject(err);
            else fulfill(state);
        });
    }

    self.prepare = function(type) {
        if (!self.db) { // wait for db connection
            setTimeout(function() {
                self.prepare(type);
            }, 30);
            return;
        }

        var indexedAndUniqueFields;
        //todo: это тут не должно быть, переделать этот свитч
        switch (type) {
            case 'sites':
                indexedAndUniqueFields = {domain: 1};
                break;

            case 'pages':
                indexedAndUniqueFields = {domain: 1, url: 1};
                break;

            case 'tags':
                indexedAndUniqueFields = {name: 1};
                break;
        }

        self.db
            .collection(type)
            .ensureIndex(indexedAndUniqueFields, {unique: true, background: true}, function() {});
    }
};

util.inherits(Mongodb, Connector);

exports.Mongodb = Mongodb;
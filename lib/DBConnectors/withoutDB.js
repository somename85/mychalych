/**
 * Created by blackbookman on 01.03.15.
 */
var util = require('util');

var Connector = require('./connector').Connector;
var f = require('../functions');

function WithoutDB() {
    var self = this;

    WithoutDB.super_.apply(self, arguments);

    self._storage = {};

    self._insert = function(type, smth, fulfill, reject) {
        self._storage[type] = self._storage[type] || [];

        if (util.isArray(smth)) {
            self._storage[type] = self._storage[type].concat(smth);
            fulfill(smth);
        } else {
            self._storage[type].push(smth);
            fulfill([smth]);
        }
    }

    self._update = function(type, obj, data, isMulti, fulfill, reject) {
        var amount = !isMulti ? 1 : 0;

        self._find(type, obj, amount, function(result) {
            var n = 0

            result.forEach(function(obj) {
                f.processObjWithRef(data, obj);
                n++;
            })

            fulfill({n: n, ok: true, updatedExisting: true});
        })
    }

    self._find = function(type, obj, amount, fulfill, reject) {
        if (!self._storage[type]) {
            reject(new Error('collection with this type not created yet'));
            return;
        };

        if (JSON.stringify(obj) == '{}') {
            fulfill(self._storage[type].slice());
            return;
        }

        var results = [],
            j = self._storage[type].length;

        loop:
        while (j--) {
            var keys = Object.keys(obj),
                i = keys.length;

            while (i--) {
                if (!self._isPropSame(obj, self._storage[type][j], keys[i])) break;
                if (i === 0) results.push(self._storage[type][j]);
                if (amount && results.length === amount) break loop;
            }
        };

        fulfill(results);
    }

    self._remove = function(type, obj, isOne, fulfill, reject) {
        if (JSON.stringify(obj) === '{}') {
            var n = self._storage[type].length;
            self._storage[type] = [];
            fulfill({ok: true, n: n});
            return;
        }

        var amount = isOne ? 1 : 0;

        self._find(type, obj, amount, function(result) {
            if (result.length === 0) {
                fulfill({ok: true, n: 0});
                return;
            }

            var n = 0;

            result.forEach(function(doc) {
                self._storage[type].splice(self._storage[type].indexOf(doc), 1);
                n++;
            })

            fulfill({ok: true, n: n});
        })
    }

    self._isPropSame = function(obj1, obj2, prop) {
        if (!(prop in obj2)) return false;
        if (obj1[prop] === obj2[prop]) return true;
        if (util.isObject(obj1[prop]) && util.isObject(obj1[prop])) {
            try {
                return JSON.stringify(obj1[prop]) === JSON.stringify(obj2[prop]);
            } catch (err) {
                console.log(err.stack);
                return false;
            }
        }
    }
}

util.inherits(WithoutDB, Connector);

exports.WithoutDB = WithoutDB;

function Old() {
    var self = this;

    Old.super_.apply(self, arguments);

    self._storage = [];

    self.find = function(smth, cb) {
        var results = [],
            indexes = [];

        if (util.isObject(smth)) {
            try {
                self._storage.forEach(function(doc, index) {
                    if (!util.isObject(doc)) return;

                    var keys = Object.keys(smth),
                        key;

                    for (var i = 0; i < keys.length; i++) {
                        key = keys[i];
                        if (!self._isPropSame(smth, doc, key)) break;

                        if (i === keys.length - 1) {
                            results.push(doc);
                            indexes.push(index);
                        }
                    }
                });
            } catch (err) {
                return func.errHandler(err, cb);
            }
        } else {
            var index = self._storage.indexOf(smth);

            if (index === -1) {
                cb();
                return;
            }
        }

        cb(null, results, indexes);
    };
    self.findAll = function(cb) {
        cb(null, self._storage.slice(0));
    };
    self._save = function(index, data, cb) {
        try {
            if (util.isNumber(index)) {
                var doc = self._storage[index];

                if (util.isObject(doc)) {
                    for (var key in data) {
                        if (!data.hasOwnProperty(key)) continue;
                        doc[key] = data[key];
                    }
                    cb(null, doc);
                } else {
                    self._storage.splice(self._storage.indexOf(doc), 1, data);
                    cb(null, data);
                }
            } else {
                self._storage.push(data);
                cb(null, data);
            }
        } catch (err) {
            return func.errHandler(err, cb);
        }
    };
    self._remove = function(index, cb) {
        if (!util.isNumber(index)) return cb(new Error('index don`t set'));
        self._storage.splice(index, 1);
        cb();
    };
    self.removeAll = function(cb) {
        self._storage = [];
        cb();
    };
}
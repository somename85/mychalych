/**
 * Created by blackbookman on 10.03.15.
 */
describe('lib', function() {
    describe('DBConnectors', function() {
        require('../../tests/lib/DBConnectors/connector');
        require('../../tests/lib/DBConnectors/mongodb');
        require('../../tests/lib/DBConnectors/withoutDB');
    });
    describe('models', function() {
        require('../../tests/lib/models/model');
        require('../../tests/lib/models/sites');
        require('../../tests/lib/models/pages');
        require('../../tests/lib/models/scripts');
        require('../../tests/lib/models/users');
        require('../../tests/lib/models/accessGroups');
        require('../../tests/lib/models/tags');
    });
    require('../../tests/lib/eventStream');
});
/**
 * Created by blackbookman on 01.03.15.
 */
var assert = require('chai').assert;
var events = require("events");

var eventStream = require('../../lib/eventStream.js').eventStream;

describe('eventStream.js', function() {
    it('should inherit form native event eventStream', function() {
        assert(eventStream instanceof events.EventEmitter);
    });

    var commands = [
        'add',
        'rm',
        'edit',
        'find',
        'success',
        'fail'
    ];

    commands.forEach(function(command) {
        it('should have ' + command + ' method and emit event with same name', function(done) {
            var fooBar = {foo: 'bar'};

            assert.ok(eventStream[command]);

            eventStream.once(command, function(data) {
                assert.deepEqual(fooBar, data.body);
                done();
            });

            eventStream[command]({type: 'foo', body: fooBar});
        });
    });
});
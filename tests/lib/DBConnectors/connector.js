/**
 * Created by blackbookman on 13.03.15.
 */
var assert = require('chai').assert;
var Connector = require('../../../lib/DBConnectors/connector').Connector;

var connector = new Connector();
var type = 'sites';

describe('connector.js', function() {
    //todo: test methods with functions and arrays as arguments??? or not
    //todo: test typeof type
    it('should not insert bad data', function(done) {
        connector
            .insert(type, 'string')
            .then(function(result) {
                assert.isNull(result);
                done();
            })
            .catch(function(err) {
                assert.instanceOf(err, Error);
                connector
                    .insert(type, 1)
                    .then(function(result) {
                        assert.isNull(result);
                        done();
                    })
                    .catch(function(err) {
                        assert.instanceOf(err, Error);
                        connector
                            .insert(type, true)
                            .then(function(result) {
                                assert.isNull(result);
                                done();
                            })
                            .catch(function(err) {
                                assert.instanceOf(err, Error);
                                done();
                            });
                    });
            });
    });

    it('should not insert several bad docs', function(done) {
        connector
            .insert(type, [{foo: 'bar'}, 1])
            .then(function(result) {
                assert.isNull(result);
                done();
            })
            .catch(function(err) {
                assert.instanceOf(err, Error);
                connector
                    .insert(type, [{foo: 'bar'}, 'string'])
                    .then(function(result) {
                        assert.isNull(result);
                        done();
                    })
                    .catch(function(err) {
                        assert.instanceOf(err, Error);
                        connector
                            .insert(type, [{foo: 'bar'}, true])
                            .then(function(result) {
                                assert.isNull(result);
                                done();
                            })
                            .catch(function(err) {
                                assert.instanceOf(err, Error);
                                done();
                            })
                    })
            })
    });

    it('should not update bad data', function(done) {
        connector
            .update(type, 1, {baz: 'baz'})
            .then(function(state) {
                assert.isNull(state.n);
                done();
            })
            .catch(function(err) {
                assert.instanceOf(err, Error);
                connector
                    .update(type, 'string', {baz: 'baz'})
                    .then(function(state) {
                        assert.isNull(state.n);
                        done();
                    })
                    .catch(function(err) {
                        assert.instanceOf(err, Error);
                        connector
                            .update(type, true, {baz: 'baz'})
                            .then(function(state) {
                                assert.isNull(state.n);
                                done();
                            })
                            .catch(function(err) {
                                assert.instanceOf(err, Error);
                                done();
                            })
                    })
            })
    });

    it('should not update several docs if data is wrong', function(done) {
        connector
            .update(type, {id: 1}, 1, true)
            .then(function(state) {
                assert.isNull(state);
                done();
            })
            .catch(function(err) {
                assert.instanceOf(err, Error);
                connector
                    .update(type, {id: 1}, 'string', true)
                    .then(function(state) {
                        assert.isNull(state);
                        done();
                    })
                    .catch(function(err) {
                        assert.instanceOf(err, Error);
                        connector
                            .update(type, {id: 1}, true, true)
                            .then(function(state) {
                                assert.isNull(state);
                                done();
                            })
                            .catch(function(err) {
                                assert.instanceOf(err, Error);
                                done();
                            })
                    })
            })
    });

    it('should not find data if query is wrong', function(done) {
        connector
            .find(type, 1)
            .then(function(result) {
                assert.isNull(result);
                done();
            })
            .catch(function(err) {
                assert.instanceOf(err, Error);
                connector
                    .find(type, 'string')
                    .then(function(result) {
                        assert.isNull(result);
                        done();
                    })
                    .catch(function(err) {
                        assert.instanceOf(err, Error);
                        connector
                            .find(type, true)
                            .then(function(result) {
                                assert.isNull(result);
                                done();
                            })
                            .catch(function(err) {
                                assert.instanceOf(err, Error);
                                done();
                            })
                    })
            })
    });

    it('should not remove data if query is wrong', function(done) {
        connector
            .remove(type, 1)
            .then(function(result) {
                assert.isNull(result);
                done();
            })
            .catch(function(err) {
                assert.instanceOf(err, Error);
                connector
                    .remove(type, 'string')
                    .then(function(result) {
                        assert.isNull(result);
                        done();
                    })
                    .catch(function(err) {
                        assert.instanceOf(err, Error);
                        connector
                            .remove(type, true)
                            .then(function(result) {
                                assert.isNull(result);
                                done();
                            })
                            .catch(function(err) {
                                assert.instanceOf(err, Error);
                                done();
                            })
                    })
            })
    });
});
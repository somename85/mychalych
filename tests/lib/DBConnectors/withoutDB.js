/**
 * Created by blackbookman on 01.03.15.
 */
var assert = require('chai').assert;

var Connector = require('../../../lib/DBConnectors/connector').Connector;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB').WithoutDB;

var db = new WithoutDB();
var type = 'sites';

describe('withoutDB.js', function() {
    beforeEach(function() {
        db._storage[type] = [];
    });

    it('should inherit from connector', function() {
        assert.instanceOf(db, Connector);
    });

    it('should insert data', function(done) {
        db
            .insert(type, {foo: 'bar'})
            .then(function(result) {
                assert.propertyVal(result[0], 'foo', 'bar');
                done();
            })
            .catch(function(err) {
                assert.equal(err, null);
                done();
            });

    });

    it('should insert several docs', function(done) {
        db
            .insert(type, [{foo: 'bar'}, {baz: 'baz'}])
            .then(function(result) {
                assert.isArray(result);
                assert.equal(result.length, 2);
                assert.propertyVal(result[0], 'foo', 'bar');
                assert.propertyVal(result[1], 'baz', 'baz');
                done();
            })
    });

    it('should update data', function(done) {
        db
            .insert(type, {foo: 'bar'})
            .then(function(result) {
                db
                    .update(type, result[0], {baz: 'baz'})
                    .then(function(state) {
                        assert.equal(state.n, 1);
                        assert.isTrue(state.ok);
                        assert.isTrue(state.updatedExisting);
                        done();
                    })
                    .catch(function(err) {
                        assert.equal(err, null);
                        done();
                    })
            })
    });

    it('should not update data, if query not fulfill', function(done) {
        db
            .update(type, {object: 'thatNotExist'}, {baz: 'baz'})
            .then(function(state) {
                assert.equal(state.n, 0);
                assert.isTrue(state.ok);
                assert.isTrue(state.updatedExisting);
                done();
            })
            .catch(function(err) {
                assert.isNull(err);
                done();
            })
    });

    it('should update several docs', function(done) {
        db
            .insert(type, [{foo: 'bar', id: 1}, {baz: 'baz', id: 1}, {la: 'la', id: 1}])
            .then(function() {
                db
                    .update(type, {id: 1}, {baz: 'baz'}, true)
                    .then(function(state) {
                        assert.equal(state.n, 3);
                        assert.isTrue(state.ok);
                        assert.isTrue(state.updatedExisting);
                        done();
                    })
                    .catch(function(err) {
                        assert.equal(err, null);
                        done();
                    })
            })
    });

    it('should find data', function(done) {
        db
            .insert(type, {foo: 'bar'})
            .then(function() {
                db
                    .find(type, {foo: 'bar'})
                    .then(function(result) {
                        assert.propertyVal(result[0], 'foo', 'bar');
                        done();
                    })
            })
    });

    it('should not find data if query is not fulfill', function(done) {
        db
            .find(type, {object: 'thatNotExist'})
            .then(function(result) {
                assert.isArray(result);
                assert.deepEqual(result, []);
                done();
            })
    });

    it('should find several docs', function(done) {
        db
            .insert(type, [{foo: 'bar', id: 1}, {baz: 'baz', id: 1}, {la: 'la', id: 1}])
            .then(function() {
                db
                    .find(type, {id: 1}, 2)
                    .then(function(result) {
                        assert.isArray(result);
                        assert.lengthOf(result, 2);
                        done();
                    })
            })
    });

    it('should find all docs that fulfill query', function(done) {
        db
            .insert(type, [{foo: 'bar', id: 1}, {baz: 'baz', id: 1}, {la: 'la', id: 1}])
            .then(function() {
                db
                    .find(type, {id: 1}, 0)
                    .then(function(result) {
                        assert.isArray(result);
                        assert.lengthOf(result, 3);
                        done();
                    })
            })
    });

    it('should find all docs', function(done) {
        db
            .insert(type, [{foo: 'bar'}, {baz: 'baz'}, {la: 'la'}])
            .then(function() {
                db
                    .find(type, {}, 0)
                    .then(function(result) {
                        assert.isArray(result);
                        assert.lengthOf(result, 3);
                        done();
                    })
            })
    });

    it('should remove data', function(done) {
        db
            .insert(type, {foo: 'bar'})
            .then(function(result) {
                db
                    .remove(type, result[0], true)
                    .then(function(result) {
                        assert.isTrue(result.ok);
                        assert.equal(result.n, 1);
                        done();
                    })
            })
    });

    it('should not remove data if query not fulfill', function(done) {
        db
            .remove(type, {object: 'thatNotExist'})
            .then(function(result) {
                assert.isTrue(result.ok);
                assert.equal(result.n, 0);
                done();
            })
    });

    it('should remove several doc', function(done) {
        db
            .insert(type, [{foo: 'bar', id: 1}, {baz: 'baz', id: 1}])
            .then(function() {
                db
                    .remove(type, {id: 1})
                    .then(function(state) {
                        assert.isTrue(state.ok);
                        assert.equal(state.n, 2);
                        done();
                    })
            })
    });

    it('should remove all doc', function(done) {
        db
            .insert(type, [{foo: 'bar'}, {baz: 'baz'}, {bar: 'foo'}])
            .then(function() {
                db
                    .remove(type, {})
                    .then(function(result) {
                        assert.isTrue(result.ok);
                        assert.equal(result.n, 3);
                        done();
                    })
            })
    });
});

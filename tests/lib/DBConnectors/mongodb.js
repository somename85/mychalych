/**
 * Created by blackbookman on 12.03.15.
 */
var assert = require('chai').assert;

var Connector = require('../../../lib/DBConnectors/connector').Connector;
var Mongodb = require('../../../lib/DBConnectors/mongodb').Mongodb;

var mongodb = new Mongodb();
var type = 'sites';

describe('mongodb.js', function() {
    beforeEach(function() {
        mongodb.remove(type, {});
    });

    after(function() {
        mongodb.db.close();
    });

    it('should inherit from connector', function() {
        assert.instanceOf(mongodb, Connector);
    });

    it('should insert data', function(done) {
        function save(mongodb) {
            mongodb
                .insert(type, {domain: 'foo'})
                .then(function(result) {
                    assert.propertyVal(result[0], 'domain', 'foo');
                    done();
                })
                .catch(function(err) {
                    if (err.message === 'db not ready yet') {
                        setTimeout(function() {
                            save(mongodb);
                        }, 10);
                    } else {
                        assert.equal(err, null);
                        done();
                    }
                });
        }

        save(mongodb);
        //todo: if script start right now, db not ready. I think i can find right way to handle this, sometime
    });

    it('should insert several docs', function(done) {
        mongodb
            .insert(type, [{domain: 'foo'}, {domain: 'bar'}])
            .then(function(result) {
                assert.isArray(result);
                assert.equal(result.length, 2);
                assert.propertyVal(result[0], 'domain', 'foo');
                assert.propertyVal(result[1], 'domain', 'bar');
                done();
            })
            .catch(function(err) {
                assert.isUndefined(err);
                done();
            });
    });

    it('should update data', function(done) {
        mongodb
            .insert(type, {domain: 'foo'})
            .then(function(result) {
                mongodb
                    .update(type, result[0], {baz: 'baz'})
                    .then(function(state) {
                        assert.equal(state.n, 1);
                        assert.isTrue(state.ok);
                        assert.isTrue(state.updatedExisting);
                        done();
                    })
                    .catch(function(err) {
                        assert.equal(err, null);
                        done();
                    })
            })
            .catch(function(err) {
                assert.isUndefined(err);
                done();
            });
    });

    it('should not update data, if query not fulfill', function(done) {
        mongodb
            .update(type, {domain: 'thatNotExist'}, {baz: 'baz'})
            .then(function(state) {
                assert.equal(state.n, 0);
                done();
            })
            .catch(function(err) {
                assert.isNull(err);
                done();
            })
    });

    it('should update several docs', function(done) {
        mongodb
            .insert(type, [{domain: 'bar', id: 1}, {domain: 'baz', id: 1}, {domain: 'la', id: 1}])
            .then(function() {
                mongodb
                    .update(type, {id: 1}, {baz: 'baz'}, true)
                    .then(function(state) {
                        assert.equal(state.n, 3);
                        assert.isTrue(state.ok);
                        assert.isTrue(state.updatedExisting);
                        done();
                    })
                    .catch(function(err) {
                        assert.equal(err, null);
                        done();
                    })
            })
            .catch(function(err) {
                assert.isUndefined(err);
                done();
            });
    });

    it('should find data', function(done) {
        mongodb
            .insert(type, {domain: 'bar'})
            .then(function() {
                mongodb
                    .find(type, {domain: 'bar'})
                    .then(function(result) {
                        assert.propertyVal(result[0], 'domain', 'bar');
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    });
            })
            .catch(function(err) {
                assert.isUndefined(err);
                done();
            });
    });

    it('should not find data if query is not fulfill', function(done) {
        mongodb
            .find(type, {domain: 'thatNotExist'})
            .then(function(result) {
                assert.isArray(result);
                assert.deepEqual(result, []);
                done();
            })
            .catch(function(err) {
                assert.isUndefined(err);
                done();
            });
    });

    it('should find several docs', function(done) {
        mongodb
            .insert(type, [{domain: 'bar', id: 1}, {domain: 'baz', id: 1}, {domain: 'la', id: 1}])
            .then(function() {
                mongodb
                    .find(type, {id: 1}, 2)
                    .then(function(result) {
                        assert.isArray(result);
                        assert.lengthOf(result, 2);
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    });
            })
            .catch(function(err) {
                assert.isUndefined(err);
                done();
            });
    });

    it('should find all docs that fulfill query', function(done) {
        mongodb
            .insert(type, [{domain: 'bar', id: 1}, {domain: 'baz', id: 1}, {domain: 'la', id: 1}])
            .then(function() {
                mongodb
                    .find(type, {id: 1}, 0)
                    .then(function(result) {
                        assert.isArray(result);
                        assert.lengthOf(result, 3);
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    });
            })
            .catch(function(err) {
                console.log(err);
                assert.isUndefined(err);
                done();
            });
    });

    it('should find all docs', function(done) {
        mongodb
            .insert(type, [{domain: 'bar'}, {domain: 'baz'}, {domain: 'la'}])
            .then(function() {
                mongodb
                    .find(type, {}, 0)
                    .then(function(result) {
                        assert.isArray(result);
                        assert.lengthOf(result, 3);
                        done();
                    })
                    .catch(function(err) {
                        console.log(err);
                        assert.isUndefined(err);
                        done();
                    });
            })
            .catch(function(err) {
                console.log(err);
                assert.isUndefined(err);
                done();
            });
    });

    it('should remove data', function(done) {
        mongodb
            .insert(type, {domain: 'bar'})
            .then(function(result) {
                mongodb
                    .remove(type, result[0])
                    .then(function(result) {
                        assert.isTrue(result.ok);
                        assert.equal(result.n, 1);
                        done();
                    })
                    .catch(function(err) {
                        console.log(err);
                        assert.isUndefined(err);
                        done();
                    });
            })
            .catch(function(err) {
                console.log(err);
                assert.isUndefined(err);
                done();
            });
    });

    it('should not remove data if query not fulfill', function(done) {
        mongodb
            .remove(type, {domain: 'thatNotExist'})
            .then(function(result) {
                assert.isTrue(result.ok);
                assert.equal(result.n, 0);
                done();
            })
            .catch(function(err) {
                console.log(err);
                assert.isUndefined(err);
                done();
            });
    });

    it('should remove several doc', function(done) {
        mongodb
            .insert(type, [{domain: 'bar', id: 1}, {domain: 'baz', id: 1}])
            .then(function() {
                mongodb
                    .remove(type, {id: 1})
                    .then(function(result) {
                        assert.isTrue(result.ok);
                        assert.equal(result.n, 2);
                        done();
                    })
                    .catch(function(err) {
                        console.log(err);
                        assert.isUndefined(err);
                        done();
                    });
            })
            .catch(function(err) {
                console.log(err);
                assert.isUndefined(err);
                done();
            });
    });

    it('should remove all doc', function(done) {
        mongodb
            .insert(type, [{domain: 'bar'}, {domain: 'baz'}, {domain: 'foo'}])
            .then(function() {
                mongodb
                    .remove(type, {})
                    .then(function(result) {
                        assert.isTrue(result.ok);
                        assert.equal(result.n, 3);
                        done();
                    })
                    .catch(function(err) {
                        console.log(err);
                        assert.isUndefined(err);
                        done();
                    });
            })
            .catch(function(err) {
                console.log(err);
                assert.isUndefined(err);
                done();
            });
    });
});
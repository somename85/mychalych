/**
 * Created by blackbookman on 22.03.15.
 */
var assert = require('chai').assert;

var Tags = require('../../../lib/models/tags.js').Tags;
var Model = require('../../../lib/models/model.js').Model;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB.js').WithoutDB;
var eventStream = require('../../../lib/eventStream.js').eventStream;

var withoutDB = new WithoutDB();
var tags = new Tags(withoutDB);


describe('tags.js', function() {
    it('should inherit from Model', function() {
        assert.ok(tags instanceof Model);
    });

    it('should know its type', function() {
        assert.equal(tags.type, 'tags');
    });

    it('should know its connector', function() {
        assert(tags.connector === withoutDB);
    });


});
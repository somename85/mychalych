/**
 * Created by blackbookman on 10.03.15.
 */
var assert = require('chai').assert;

var Users = require('../../../lib/models/users').Users;
var Model = require('../../../lib/models/model').Model;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB').WithoutDB;
var eventStream = require('../../../lib/eventStream').eventStream;

var withoutDB = new WithoutDB();
var users = new Users(withoutDB);


describe('users.js', function() {
    it('should inherit from Model', function() {
        assert.ok(users instanceof Model);
    });

    it('should know its type', function() {
        assert.equal(users.type, 'users');
    });

    it('should know its connector', function() {
        assert.equal(users.connector, withoutDB);
    });
});
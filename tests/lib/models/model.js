/**
* Created by blackbookman on 10.03.15.
*/
var assert = require('chai').assert;

var Model = require('../../../lib/models/model').Model;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB').WithoutDB;
var Mongodb = require('../../../lib/DBConnectors/mongodb').Mongodb;
var eventStream = require('../../../lib/eventStream').eventStream;
var f = require('../../../lib/functions');

var withoutDB = new WithoutDB();
var mongodb = new Mongodb();
var models = new Model(withoutDB);
var type = 'models';

var fooBar = {foo: 'bar', id: 1};
var fooBarArr = [{foo: 'bar', id: 1}, {foo: 'bar', id: 2}, {foo: 'bar', id: 3}];

models.subscribe();

describe('model.js', function() {
    beforeEach(function() {
        models.connector.remove(type, {});
        models.eventStream
            .removeAllListeners('success')
            .removeAllListeners('fail');
    });

    it('should know about eventStream', function() {
        assert.strictEqual(models.eventStream, eventStream);
    });

    it('should can add 1 item (eventStream)', function(done) {
        eventStream
            .on('success', function(data) {
                if (!f.cd(data, type, 'add')) return;
                assert.deepEqual(fooBar, data.body[0]);
                done();
            })
            .add({type: type, body: fooBar});
    });

    it('should can add 1 item', function(done) {
        models
            .add({type: type, body: fooBar})
            .then(function(result) {
                assert.deepEqual(fooBar, result[0]);
                done();
            });
    });

    it('should can add several items (eventStream)', function(done) {
        eventStream
            .on('success', function(data) {
                if (!f.cd(data, type)) return;
                assert.equal('add', data.command);
                assert.lengthOf(data.body, 3);
                assert.propertyVal(data.body[0], 'foo', 'bar');
                assert.propertyVal(data.body[1], 'foo', 'bar');
                assert.propertyVal(data.body[2], 'foo', 'bar');
                done();
            })
            .add({type: type, body: fooBarArr});
    });

    it('should can add several items', function(done) {
        models
            .add({type: type, body: fooBarArr})
            .then(function(result) {
                assert.lengthOf(result, 3);
                assert.propertyVal(result[0], 'foo', 'bar');
                assert.propertyVal(result[1], 'foo', 'bar');
                assert.propertyVal(result[2], 'foo', 'bar');
                done();
            });
    });

    it('should can edit 1 item (eventStream)', function(done) {
        var body = [
            {id: 1},
            {'baz': 'baz'}
        ];

        eventStream
            .add({type: type, body: fooBar})
            .on('success', function(state) {
                if (!f.cd(state, type, 'edit')) return;
                assert.equal(state.body.n, 1);
                assert.isTrue(state.body.updatedExisting);
                done();
            })
            .edit({
                type: type,
                body: body
            });
    });

    it('should can edit 1 item', function(done) {
        var body = [
            {id: 1},
            {'baz': 'baz'}
        ];

        models
            .add({type: type, body: fooBar})
            .then(function() {
                models
                    .edit({
                        type: type,
                        body: body
                    })
                    .then(function(state) {
                        assert.equal(state.n, 1);
                        assert.isTrue(state.updatedExisting);
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    });
            })
    });

    it('should can edit all items, that fulfill query (eventStream)', function(done) {
        var body = [
            {foo: 'bar'},
            {'baz': 'baz'},
            true
        ];

        eventStream
            .add({type: type, body: fooBarArr})
            .on('success', function(state) {
                if (!f.cd(state, type, 'edit')) return;
                assert.equal(state.body.n, 3);
                assert.isTrue(state.body.updatedExisting);
                done();
            })
            .edit({type: type, body: body});
    });

    it('should can edit all items, that fulfill query', function(done) {
        var body = [
            {foo: 'bar'},
            {'baz': 'baz'},
            true
        ];

        models
            .add({type: type, body: fooBarArr})
            .then(function() {
                models
                    .edit({
                        type: type,
                        body: body
                    })
                    .then(function(state) {
                        assert.equal(state.n, 3);
                        assert.isTrue(state.updatedExisting);
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    });
            })
    });

    it('should can find all items, that fulfill query (eventStream)', function(done) {
        eventStream
            .add({type: type, body: fooBarArr})
            .on('success', function(data) {
                if (!f.cd(data, type, 'find')) return;
                assert.lengthOf(data.body, 3);
                assert.propertyVal(data.body[0], 'foo', 'bar');
                assert.propertyVal(data.body[1], 'foo', 'bar');
                assert.propertyVal(data.body[2], 'foo', 'bar');
                done();
            })
            .find({type: type, body: [{foo: 'bar'}, 0]});
    });

    it('should can find all items, that fulfill query', function(done) {
        models
            .add({type: type, body: fooBarArr})
            .then(function() {
                models
                    .find({type: type, body: [{foo: 'bar'}, 0]})
                    .then(function(result) {
                        assert.lengthOf(result, 3);
                        assert.propertyVal(result[0], 'foo', 'bar');
                        assert.propertyVal(result[1], 'foo', 'bar');
                        assert.propertyVal(result[2], 'foo', 'bar');
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    });
            });
    });

    it('should can find 1 item (eventStream)', function(done) {
        eventStream
            .add({type: type, body: fooBar})
            .on('success', function(data) {
                if (!f.cd(data, type, 'find')) return;
                assert.lengthOf(data.body, 1);
                assert.propertyVal(data.body[0], 'foo', 'bar');
                done();
            })
            .find({type: type, body: [{id: 1}]});
    });

    it('should can find 1 item', function(done) {
        models
            .add({type: type, body: fooBar})
            .then(function() {
                models
                    .find({type: type, body: [{id: 1}]})
                    .then(function(result) {
                        assert.lengthOf(result, 1);
                        assert.propertyVal(result[0], 'foo', 'bar');
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    })
            })
    });

    it('should can find several items (eventStream)', function(done) {
        eventStream
            .add({type: type, body: fooBarArr})
            .on('success', function(data) {
                if (!f.cd(data, type, 'find')) return;
                assert.lengthOf(data.body, 2);
                assert.propertyVal(data.body[0], 'foo', 'bar');
                assert.propertyVal(data.body[1], 'foo', 'bar');
                done();
            })
            .find({type: type, body: [{foo: 'bar'}, 2]});
    });

    it('should can find several items', function(done) {
        models
            .add({type: type, body: fooBarArr})
            .then(function() {
                models
                    .find({type: type, body: [{foo: 'bar'}, 2]})
                    .then(function(result) {
                        assert.lengthOf(result, 2);
                        assert.propertyVal(result[0], 'foo', 'bar');
                        assert.propertyVal(result[1], 'foo', 'bar');
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    })
            })
    });

    it('should can find all items (eventStream)', function(done) {
        eventStream
            .add({type: type, body: fooBarArr})
            .on('success', function(data) {
                if (!f.cd(data, type, 'find')) return;
                assert.lengthOf(data.body, 3);
                assert.propertyVal(data.body[0], 'foo', 'bar');
                assert.propertyVal(data.body[1], 'foo', 'bar');
                assert.propertyVal(data.body[2], 'foo', 'bar');
                done();
            })
            .find({type: type});
    });

    it('should can find all items', function(done) {
        models
            .add({type: type, body: fooBarArr})
            .then(function() {
                models
                    .find({type: type})
                    .then(function(result) {
                        assert.lengthOf(result, 3);
                        assert.propertyVal(result[0], 'foo', 'bar');
                        assert.propertyVal(result[1], 'foo', 'bar');
                        assert.propertyVal(result[2], 'foo', 'bar');
                        done();
                    })
                    .catch(function(err) {
                        assert.isUndefined(err);
                        done();
                    })
            })
    });

    it('should can remove 1 item (eventStream)', function(done) {
        eventStream
            .add({type: type, body: fooBarArr})
            .on('success', function(data) {
                if (!f.cd(data, type, 'rm')) return;
                assert.isTrue(data.body.ok);
                assert.equal(data.body.n, 1);
                done();
            })
            .on('fail', function(data) {
                if (!f.cd(data, type, 'rm')) return;
                assert.isUndefined(data);
                done();
            })
            .rm({type: type, body: [{id: 2}, true]});
    });

    it('should can remove 1 item', function(done) {
        models
            .add({type: type, body: fooBarArr})
            .then(function() {
                models
                    .rm({type: type, body: [{id: 2}, true]})
                    .then(function(state) {
                        assert.isTrue(state.ok);
                        assert.equal(state.n, 1);
                        done();
                    }, function(err) {
                        assert.isUndefined(err);
                        done();
                    })

            })
    });

    it('should can remove all items, that fulfill query (eventStream)', function(done) {
        eventStream
            .add({type: type, body: [{foo: 'bar', id: 2}, {baz: 'baz', id: 2}, {q: 'q'}]})
            .on('success', function(data) {
                if (!f.cd(data, type, 'rm')) return;
                assert.isTrue(data.body.ok);
                assert.equal(data.body.n, 2);
                done();
            })
            .on('fail', function(data) {
                if (!f.cd(data, type, 'rm')) return;
                assert.isUndefined(data);
                done();
            })
            .rm({type: type, body: [{id: 2}]});
    });

    it('should can remove all items, that fulfill query', function(done) {
        models
            .add({type: type, body: [{foo: 'bar', id: 2}, {baz: 'baz', id: 2}, {q: 'q'}]})
            .then(function() {
                models
                    .rm({type: type, body: [{id: 2}]})
                    .then(function(state) {
                        assert.isTrue(state.ok);
                        assert.equal(state.n, 2);
                        done();
                    }, function(err) {
                        assert.isUndefined(err);
                        done();
                    })

            })
    });

    it('should can remove all items', function(done) {
        models
            .add({type: type, body: fooBarArr})
            .then(function() {
                models
                    .rm({type: type})
                    .then(function(state) {
                        assert.isTrue(state.ok);
                        assert.equal(state.n, 3);
                        done();
                    }, function(err) {
                        assert.isUndefined(err);
                        done();
                    })

            })
    });

    it('should can remove all items (eventStream)', function(done) {
        eventStream
            .add({type: type, body: fooBarArr})
            .on('success', function(data) {
                if (!f.cd(data, type, 'rm')) return;
                assert.isTrue(data.body.ok);
                assert.equal(data.body.n, 3);
                done();
            })
            .on('fail', function(data) {
                if (!f.cd(data, type, 'rm')) return;
                assert.isUndefined(data);
                done();
            })
            .rm({type: type});
    });

    it('should trigger fail event when arguments are wrong (add) (eventStream)', function(done) {
        var i = 0;
        eventStream
            .on('fail', function(data) {
                //here we should check data.type and data.command,
                //but it is error testing, so data.type=null sometimes
                i++;
                assert.ok(data.body instanceof Error);
            })
            .add(true)
            .add({foo: 'bar'})
            .add({type: type, body: true})
            .add({type: type, body: 1})
            .add({type: type, body: 'string'})
            .cb(function() {
                assert.equal(i, 5);
                done();
            });
    });

    it('should trigger fail event when arguments are wrong (edit) (eventStream)', function(done) {
        var i = 0;
        eventStream
            .on('fail', function(data) {
                i++;
                if (f.cd(data, type, 'edit')) assert.ok(data.body instanceof Error);
            })
            .edit(true)
            .edit({foo: 'bar'})
            .edit({type: type, body: true})
            .edit({type: type, body: {foo: 'bar'}})
            .edit({type: type, body: [{foo: 'bar'}, 1]})
            .edit({type: type, body: [0, {foo: 'bar'}]})
            .cb(function() {
                assert.equal(i, 6);
                done();
            });
    });

    it('should trigger fail event when arguments are wrong (find) (eventStream)', function(done) {
        var i = 0;
        eventStream
            .on('fail', function(data) {
                i++;
                if (f.cd(data, type, 'find')) assert.ok(data.body instanceof Error);
            })
            .find(true)
            .find({foo: 'bar'})
            .find({type: type, body: true})
            .cb(function() {
                assert.equal(i, 3);
                done();
            });
    });

    it('should trigger fail event when arguments are wrong(rm) (eventStream)', function(done) {
        var i = 0;
        eventStream
            .on('fail', function(data) {
                i++;
                if (f.cd(data, type, 'rm')) assert.ok(data.body instanceof Error);
            })
            .rm(true)
            .rm({foo: 'bar'})
            .rm({type: type, body: true})
            .cb(function() {
                assert.equal(i, 3);
                done();
            });
    });

    it('should not be subscribed on eventStream, when created', function() {
        var newModels = new Model(WithoutDB);
        assert.isFalse(newModels.subscribeStatus);

        newModels.subscribe();
        assert.isTrue(newModels.subscribeStatus);
    });
});
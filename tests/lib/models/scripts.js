/**
 * Created by blackbookman on 09.03.15.
 */
var assert = require('chai').assert;

var Scripts = require('../../../lib/models/scripts').Scripts;
var Model = require('../../../lib/models/model').Model;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB').WithoutDB;
var eventStream = require('../../../lib/eventStream').eventStream;

var withoutDB = new WithoutDB();
var scripts = new Scripts(withoutDB);


describe('scripts.js', function() {
    it('should inherit from Model', function() {
        assert.ok(scripts instanceof Model);
    });

    it('should know its type', function() {
        assert.equal(scripts.type, 'scripts');
    });

    it('should know its connector', function() {
        assert(scripts.connector === withoutDB);
    });
});
/**
 * Created by blackbookman on 09.03.15.
 */
var assert = require('chai').assert;

var Pages = require('../../../lib/models/pages').Pages;
var Model = require('../../../lib/models/model').Model;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB').WithoutDB;
var eventStream = require('../../../lib/eventStream').eventStream;

var withoutDB = new WithoutDB();
var pages = new Pages(withoutDB);
var type = 'pages';


describe('pages.js', function() {
    it('should inherit from Model', function() {
        assert.instanceOf(pages, Model);
    });

    it('should know its type', function() {
        assert.equal(pages.type, type);
    });

    it('should know its connector', function() {
        assert.equal(pages.connector, withoutDB);
    });

    it('should have defaults', function() {
        assert.deepEqual(pages.defaults, {tags: [], enable: false});
    })
});
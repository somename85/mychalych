/**
 * Created by blackbookman on 10.03.15.
 */
var assert = require('chai').assert;

var AccessGroups = require('../../../lib/models/accessGroups').AccessGroups;
var Model = require('../../../lib/models/model').Model;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB').WithoutDB;
var eventStream = require('../../../lib/eventStream').eventStream;

var withoutDB = new WithoutDB();
var accessGroups = new AccessGroups(withoutDB);


describe('accessGroups.js', function() {
    it('should inherit from Model', function() {
        assert.ok(accessGroups instanceof Model);
    });

    it('should know its type', function() {
        assert.equal(accessGroups.type, 'accessGroups');
    });

    it('should know its connector', function() {
        assert.equal(accessGroups.connector, withoutDB);
    });
});
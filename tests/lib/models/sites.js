/**
 * Created by blackbookman on 01.03.15.
 */
var assert = require('chai').assert;

var Sites = require('../../../lib/models/sites.js').Sites;
var Model = require('../../../lib/models/model.js').Model;
var WithoutDB = require('../../../lib/DBConnectors/withoutDB.js').WithoutDB;
var eventStream = require('../../../lib/eventStream.js').eventStream;

var connector = new WithoutDB();
var type = 'sites';
var sites = new Sites(connector);


describe('sites.js', function() {
    it('should inherit from Model', function() {
        assert.ok(sites instanceof Model);
    });

    it('should know its type', function() {
        assert.equal(sites.type, type);
    });

    it('should know its connector', function() {
        assert.equal(sites.connector, connector);
    });

    it('should have defaults', function() {
        assert.deepEqual(sites.defaults, {tags: [], enable: false});
    });

    it('should be subscribed on eventStream, when created', function() {
        assert.isTrue(sites.subscribeStatus);
    });
});